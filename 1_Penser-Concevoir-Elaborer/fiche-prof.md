Thématique : Tableur 'excel'

Notions liées : Données en table excel

Résumé de l’activité : Activité pour le calcul et l'analyse de moyenne des notes. 

Objectifs : Identifier et manipuler les différents éléments de l'environnement de travail du tableur excel 

Auteur : Nassima Lembarky

Durée de l’activité : 2h

Forme de participation : individuelle ou en binôme

Matériel nécessaire : Ordinateur 

Préparation : Aucune

Autres références :

Fiche élève cours :

Fiche élève activité : Disponible [ici] (https://gitlab.com/nassimalembarky/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/activit%C3%A9_%C3%A9l%C3%A8ve.pdf)
